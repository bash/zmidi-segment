Segmenting MidiFiles by Repeating Patterns
================================================================================

This `README` describes the segmentation executable `midiseg`. `midiseg` is 
a simple tool that detects the repeated pattens in a monophonic MIDI file and 
uses the these patterns to create a segmentation.

Within `midiseg` a musical note can be represented in different ways, e.g. 
pitch class, inter-onset interval etc. Given a melody we obtain a segmentation 
by constructing a suffix tree and pruning the non-left diverse nodes. Only the 
root of the suffix tree has more than two subtrees, representing sequences that
start with different a different (view of a) note. All sequences represented by
such a subtree share a common prefix. However, they do not necessarily have to 
represent identical repeated patterns, they can have different suffixes. 
Furthermore, the left-diversity of these subtrees ensures that these subtrees 
do not share a suffix, unless this suffix is repeated. Because sequences 
extracted from the same subtree are _related_ by common prefix, we assign a 
unique label (`A .. Z`) to every subtree below the root, which we name _subtree
label_ and will become the part label used in the final segmentation. To be 
able to track all exact repetitions, we also assign a second label (`0 .. 
100`), which we name _repetition number_, to every exact repetition encountered
when further traversing the subtree. Sometimes a `*` branch label occurs, this
label represents a section that is not repeated anywhere in the song. 
The combination of the subtree label and the repetition number can uniquely 
identify the repeated pattern.

`midiseg` extracts the repeated sequences of (views of) notes from the suffix 
tree. Next, a segmentation is obtained by segmenting the melody at every 
position where a repeated pattern starts and a subtree label changes. A brief 
statistical analysis of the repeated sequences a dataset of chord sequences 
indicated that segmentation boundaries are more likely to occur at repetition 
starts than at repetition endings. Using the change of branch label instead of 
the branch label _and_ repetition number for indicating segment boundaries 
creates a more robust form of segmentation, since the repetitions sharing a 
branch label are related, but not necessarily identical.


### Executable
The executable is called `midiseg` and has the following options:

```
usage: midiseg [options]
  -m,--mode <mode>         The operation mode (print|segment|reppat
                           |notes|boundary)
  -v,--view <view>         The view (ioi|contour|pc|pitch|interval
                            |ioirat|pchXioi|intXioir)
  [-r,--rep <rep>]         Repetition kind (max|supermax). Default: max
  [-p,--rem-olp <yes/no>]  REMOVE overlapping repetitions (no|yes) Default: no
  [-l,--length <length>]   Minimal repetition length Default: 0
  [-o,--out <filepath>]    Output directory for the segmentation files
  [-f,--file <filepath>]   Input file midi file to analyse
  [-d,--dir <filepath>]    Base directory path containing only MIDI files
  [-b,--bound <filepath>]  Selects the kind of boundary (start|end|both)
                           in boundary mode
```

If you call `midiseg` without any arguments this list will be printed to 
the console.


### Prerequisites 
`midiseg` only accepts monophonic MIDI files. If you present it a polyphonic
MIDI file, it will throw an error.


### Options
There are quite a few options that can be explored. The user has to specify
at least the `--mode`, `--view`, and either a `--file` or `-dir`.

#### Mode
The required argument `--mode` or `-m` specifies the mode of operation. There 
are three kinds of output:

1. `print`: a textual representation of the repetitions.
2. `reppat`: a segmentations based on the branch labels.
3. `segment`: a binary segmentation representation that prints a `1` or `0` on 
.. each line where each line represents a midi note.

#### View (point) or Feature
`midiseg` can use a number of different features (view points), which are
controlled by the argument `--view` or `-v`:

1. `ioi`: Inter-Onset Intervals.
2. `contour`: melodic contour, reducing melodic intervals to an alphabet of 
.. step, leap and prime (`-s,+s,-l,+l,p`), where the first two have a direction.
3. `pc`: pitch class.
4. `pitch`: absolute pitch is represented as a tuple of an octave and a pitch
.. class.
5. `interval`: the relative intervals between successive pitches.
6. `ioirat`: Inter-onset-interval ratios, the ratio between successive inter-
.. onset intervals.

#### Repetition kind
`midiseg` can detect two kinds of repetitions controlled by the argument 
`--rep`. If no argument is provided, `midiseg` uses maximal repetitions.

* `max`: maximal repeated patterns. Maximal repeats are pairs of repeated 
.. patterns that, if extended to either the left of right side, break their 
.. equality.
* `supermax`: super maximal repeated patterns. `midiseg` will use super maximal
.. repetitions for its segmentation procedure. A super maximal repeat is a
.. maximal repeat that is not a substring of any other maximal repeat.

#### Overlapping repetitions 
As you can see with the `reppat` mode of operation, the repetitions that are 
found will very often overlap. The user can choose to remove the overlap in
repetitions with the argument `--rem-olp` or `-p`. By default the overlapping 
repeated patterns are allowed, and every repetition -- overlapping or not -- 
where a _branch label_ changes will introduce a segment boundary. 

* `yes`: Allow overlapping repetitions. In this mode _non repeated_ patterns
.. might be indicated by a `('*',0)` label. These non repeated patterns fill the
.. holes in the melodic sequence and are ignored by the segmentation process.
* `no`: Repetitions with the same _branch label_ are merged. Also, the 
.. repetitions are extended towards the beginning and end of the piece. 
.. Furthermore, the repetitions labels are relabelled to have a more simple and
.. readable labelling.

N.B. because both modes use a branch label changes for introducing segment 
boundaries, the differences in segmentation will be different. The main
differences will occur at the beginning and end of the pieces.

#### File and Directory Input and Output
To run `midiseg` file or directory input has to be provided using `--file`, 
`-f`, `--dir`, or `-d`. If a file is presented the file will be analysed and 
repetition and segmentation information will be printed to the console. If a
directory is provided, a segmentation will be written to a new file printing a
`1` or `0` at every line, when using `--mode segment`. A `1` indicates a segment
boundary and a `0` indicates no boundary. Every line represents a note event.

With the option `--out`, or `-o` an output directory can be specified to which
the segmentation files will be written.

### Installing
Installing is not necessary. Just run the executable.

### Licence
`midiseg` is written by W.Bas de Haas and is copyrighted by Utrecht University
2012--2014. 

This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more 
details.

You should have received a copy of the GNU General Public License along with 
this program.  If not, see <http://www.gnu.org/licenses/>.

### Contact
If you have further questions, please contact:
W. Bas de Haas <w.b.dehaas@uu.nl>

### References
The segmentations strategies in `midiseg` are based on the segmentation 
ideas in the following paper. If you use this program in a scientific context,
please cite or refer to:

W. Bas de Haas, Anja Volk, Frans Wiering. "Structural segmentation of music 
based on repeated harmonies", In: _Proceedings of the IEEE International 
Symposium on Multimedia_ (2013) 255--258.
