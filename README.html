<h1>Segmenting MidiFiles by Repeating Patterns</h1>

<p>This <code>README</code> describes the segmentation executable <code>midiseg</code>. <code>midiseg</code> is 
a simple tool that detects the repeated pattens in a monophonic MIDI file and 
uses the these patterns to create a segmentation.</p>

<p>Within <code>midiseg</code> a musical note can be represented in different ways, e.g. 
pitch class, inter-onset interval etc. Given a melody we obtain a segmentation 
by constructing a suffix tree and pruning the non-left diverse nodes. Only the 
root of the suffix tree has more than two subtrees, representing sequences that
start with different a different (view of a) note. All sequences represented by
such a subtree share a common prefix. However, they do not necessarily have to 
represent identical repeated patterns, they can have different suffixes. 
Furthermore, the left-diversity of these subtrees ensures that these subtrees 
do not share a suffix, unless this suffix is repeated. Because sequences 
extracted from the same subtree are <em>related</em> by common prefix, we assign a 
unique label (<code>A .. Z</code>) to every subtree below the root, which we name <em>subtree
label</em> and will become the part label used in the final segmentation. To be 
able to track all exact repetitions, we also assign a second label (<code>0 .. 
100</code>), which we name <em>repetition number</em>, to every exact repetition encountered
when further traversing the subtree. Sometimes a <code>*</code> branch label occurs, this
label represents a section that is not repeated anywhere in the song. 
The combination of the subtree label and the repetition number can uniquely 
identify the repeated pattern.</p>

<p><code>midiseg</code> extracts the repeated sequences of (views of) notes from the suffix 
tree. Next, a segmentation is obtained by segmenting the melody at every 
position where a repeated pattern starts and a subtree label changes. A brief 
statistical analysis of the repeated sequences a dataset of chord sequences 
indicated that segmentation boundaries are more likely to occur at repetition 
starts than at repetition endings. Using the change of branch label instead of 
the branch label <em>and</em> repetition number for indicating segment boundaries 
creates a more robust form of segmentation, since the repetitions sharing a 
branch label are related, but not necessarily identical.</p>

<h3>Executable</h3>

<p>The executable is called <code>midiseg</code> and has the following options:</p>

<p><code><pre>
usage: midiseg [options]
  -m,--mode &lt;mode&gt;         The operation mode (print|segment|reppat
                           |notes|boundary)
  -v,--view &lt;view&gt;         The view (ioi|contour|pc|pitch|interval
                            |ioirat|pchXioi|intXioir)
  [-r,--rep &lt;rep&gt;]         Repetition kind (max|supermax). Default: max
  [-p,--rem-olp &lt;yes/no&gt;]  REMOVE overlapping repetitions (no|yes) Default: no
  [-l,--length &lt;length&gt;]   Minimal repetition length Default: 0
  [-o,--out &lt;filepath&gt;]    Output directory for the segmentation files
  [-f,--file &lt;filepath&gt;]   Input file midi file to analyse
  [-d,--dir &lt;filepath&gt;]    Base directory path containing only MIDI files
  [-b,--bound &lt;filepath&gt;]  Selects the kind of boundary (start|end|both)
                           in boundary mode
</pre></code></p>

<p>If you call <code>midiseg</code> without any arguments this list will be printed to 
the console.</p>

<h3>Prerequisites</h3>

<p><code>midiseg</code> only accepts monophonic MIDI files. If you present it a polyphonic
MIDI file, it will throw an error.</p>

<h3>Options</h3>

<p>There are quite a few options that can be explored. The user has to specify
at least the <code>--mode</code>, <code>--view</code>, and either a <code>--file</code> or <code>-dir</code>.</p>

<h4>Mode</h4>

<p>The required argument <code>--mode</code> or <code>-m</code> specifies the mode of operation. There 
are three kinds of output:</p>

<ol>
<li><code>print</code>: a textual representation of the repetitions.</li>
<li><code>reppat</code>: a segmentations based on the branch labels.</li>
<li><code>segment</code>: a binary segmentation representation that prints a <code>1</code> or <code>0</code> on 
.. each line where each line represents a midi note.</li>
</ol>

<h4>View (point) or Feature</h4>

<p><code>midiseg</code> can use a number of different features (view points), which are
controlled by the argument <code>--view</code> or <code>-v</code>:</p>

<ol>
<li><code>ioi</code>: Inter-Onset Intervals.</li>
<li><code>contour</code>: melodic contour, reducing melodic intervals to an alphabet of 
.. step, leap and prime (<code>-s,+s,-l,+l,p</code>), where the first two have a direction.</li>
<li><code>pc</code>: pitch class.</li>
<li><code>pitch</code>: absolute pitch is represented as a tuple of an octave and a pitch
.. class.</li>
<li><code>interval</code>: the relative intervals between successive pitches.</li>
<li><code>ioirat</code>: Inter-onset-interval ratios, the ratio between successive inter-
.. onset intervals.</li>
</ol>

<h4>Repetition kind</h4>

<p><code>midiseg</code> can detect two kinds of repetitions controlled by the argument 
<code>--rep</code>. If no argument is provided, <code>midiseg</code> uses maximal repetitions.</p>

<ul>
<li><code>max</code>: maximal repeated patterns. Maximal repeats are pairs of repeated 
.. patterns that, if extended to either the left of right side, break their 
.. equality.</li>
<li><code>supermax</code>: super maximal repeated patterns. <code>midiseg</code> will use super maximal
.. repetitions for its segmentation procedure. A super maximal repeat is a
.. maximal repeat that is not a substring of any other maximal repeat.</li>
</ul>

<h4>Overlapping repetitions</h4>

<p>As you can see with the <code>reppat</code> mode of operation, the repetitions that are 
found will very often overlap. The user can choose to remove the overlap in
repetitions with the argument <code>--rem-olp</code> or <code>-p</code>. By default the overlapping 
repeated patterns are allowed, and every repetition -- overlapping or not -- 
where a <em>branch label</em> changes will introduce a segment boundary. </p>

<ul>
<li><code>yes</code>: Allow overlapping repetitions. In this mode <em>non repeated</em> patterns
.. might be indicated by a <code>('*',0)</code> label. These non repeated patterns fill the
.. holes in the melodic sequence and are ignored by the segmentation process.</li>
<li><code>no</code>: Repetitions with the same <em>branch label</em> are merged. Also, the 
.. repetitions are extended towards the beginning and end of the piece. 
.. Furthermore, the repetitions labels are relabelled to have a more simple and
.. readable labelling.</li>
</ul>

<p>N.B. because both modes use a branch label changes for introducing segment 
boundaries, the differences in segmentation will be different. The main
differences will occur at the beginning and end of the pieces.</p>

<h4>File and Directory Input and Output</h4>

<p>To run <code>midiseg</code> file or directory input has to be provided using <code>--file</code>, 
<code>-f</code>, <code>--dir</code>, or <code>-d</code>. If a file is presented the file will be analysed and 
repetition and segmentation information will be printed to the console. If a
directory is provided, a segmentation will be written to a new file printing a
<code>1</code> or <code>0</code> at every line, when using <code>--mode segment</code>. A <code>1</code> indicates a segment
boundary and a <code>0</code> indicates no boundary. Every line represents a note event.</p>

<p>With the option <code>--out</code>, or <code>-o</code> an output directory can be specified to which
the segmentation files will be written.</p>

<h3>Installing</h3>

<p>Installing is not necessary. Just run the executable.</p>

<h3>Licence</h3>

<p><code>midiseg</code> is written by W.Bas de Haas and is copyrighted by Utrecht University
2012--2014. </p>

<p>This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU Lesser General Public License as published by the Free 
Software Foundation, either version 3 of the License.</p>

<p>This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more 
details.</p>

<p>You should have received a copy of the GNU General Public License along with 
this program.  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.</p>

<h3>Contact</h3>

<p>If you have further questions, please contact:
W. Bas de Haas <a href="m&#97;&#x69;&#x6C;&#x74;&#111;:&#119;&#x2E;b&#x2E;&#100;&#101;&#x68;&#x61;&#x61;&#x73;&#64;&#x75;&#117;&#46;&#110;&#108;">&#119;&#x2E;b&#x2E;&#100;&#101;&#x68;&#x61;&#x61;&#x73;&#64;&#x75;&#117;&#46;&#110;&#108;</a></p>

<h3>References</h3>

<p>The segmentations strategies in <code>midiseg</code> are based on the segmentation 
ideas in the following paper. If you use this program in a scientific context,
please cite or refer to:</p>

<p>W. Bas de Haas, Anja Volk, Frans Wiering. "Structural segmentation of music 
based on repeated harmonies", In: <em>Proceedings of the IEEE International 
Symposium on Multimedia</em> (2013) 255--258.</p>

