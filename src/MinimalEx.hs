{-# OPTIONS_GHC -Wall                   #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE GADTs                      #-}
module MinimalEx where

import Data.List          ( intercalate )

--------------------------------------------------------------------------------
-- A highly simplified MIDI representation
--------------------------------------------------------------------------------

-- | Assume for now that Midi information consists of Time and Pitch information
data MidiEvent = MidiEvent { midiOnset :: Time
                           , midiPitch :: Pitch
                           } deriving (Eq, Ord)
                           
-- | Representing Musical time discretely
newtype Time   = Time   { time   :: Int } 
  deriving ( Eq, Show, Num, Ord, Enum, Real, Integral )

-- | Representing Musical pitch discretely. The first 'Int' represent the octave
-- the second 'Int' represents the pitch class
newtype Pitch  = Pitch  { pitch  :: (Int, Int) }
  deriving ( Eq, Show, Ord )
  
--------------------------------------------------------------------------------
-- A more flexible representation for representing features / views derived
--------------------------------------------------------------------------------  

-- | A not consist of some specific view, or feature representation, derived 
-- from a 'MidiEvent', an onset, and an index
data Note v where 
  Note :: View v => { nview     :: v
                    , nonset    :: Time
                    , nNoteIx   :: NoteIx
                    } -> Note v 

instance Show (Note v) where
  show (Note v t ix) = show ix ++ " " ++ show t ++ ": " ++ show v 
  showList l s = ( intercalate "\n" . map show $ l ) ++ s

                    
-- | The index of the 'Note' in a 'Tune'
newtype NoteIx = NoteIx { noteIx :: Int } 
  deriving ( Eq, Show, Num, Ord, Enum, Real, Integral )
                             
                             
-- Some views on 'MidiEvent' data, 
-- | A musical interval
-- newtype Interval = Interval { interval :: Int   } 
  -- deriving ( Eq, Show, Num, Ord, Enum, Real, Integral )
  
-- | An Inter Onset Interval, ie. the time between two onsets
newtype IOI      = IOI      { ioi      :: Int   } 
  deriving ( Eq, Show, Num, Ord, Enum, Real, Integral )
  
-- | A Pitch class
newtype PC       = PC       { pc       :: Int   } 
  deriving ( Eq, Show, Num, Ord, Enum, Real, Integral )

data MyView = IOIView IOI | PCView  PC deriving Show

class Show v => View v where
  select :: v -> [MidiEvent] -> [Note v]
    
instance View MyView where
  select (PCView  _) = toSingleViews pcView
  select (IOIView _) = toCombViews ioiView

    
-- class SingleView v where
  -- toSingleView :: MidiEvent -> v
   
-- class CombView v where
  -- toCombView :: MidiEvent -> MidiEvent -> v

    
-- class GView = 
-- class HasSingleView c v where
  -- toSingleViews' :: (MidiEvent -> v) -> [MidiEvent] -> [c v]
  
-- class HasDoublView c v where 
  -- toCombViews' :: (MidiEvent -> MidiEvent -> v) -> [MidiEvent] -> [c v]
--------------------------------------------------------------------------------
-- View instances
--------------------------------------------------------------------------------

pcView :: MidiEvent -> MyView
pcView = PCView . PC . snd . pitch . midiPitch

ioiView :: MidiEvent -> MidiEvent -> MyView
ioiView a b = IOIView . IOI . time $ (midiOnset b - midiOnset a)

toSingleViews :: forall v. View v => (MidiEvent -> v) -> [MidiEvent] -> [Note v]
toSingleViews f = zipWith toSingleNote [0..] where

  toSingleNote :: NoteIx -> MidiEvent -> Note v
  toSingleNote i e = Note (f e) (midiOnset e) i 
  
-- |   
toCombViews :: forall v. View v => (MidiEvent -> MidiEvent -> v) -> [MidiEvent] -> [Note v]
toCombViews g s = toCombViews' 1 s where

  toCombViews' :: NoteIx -> [MidiEvent] -> [Note v]
  toCombViews' i [f , t]     = [toCombNote i f t]
  toCombViews' i (f : t : r) =  toCombNote i f t : toCombViews' (succ i) (t : r)
  toCombViews' _ _ = error "cannot calculate CombView for less than two notes" 

  toCombNote :: View v => NoteIx -> MidiEvent -> MidiEvent -> Note v
  toCombNote i fr to = Note (g fr to) (midiOnset fr) i 


--------------------------------------------------------------------------------
-- Testing
--------------------------------------------------------------------------------

-- | Some test data 
testTune :: [MidiEvent]
testTune = [ MidiEvent (Time 0) (Pitch (4, 1))
           , MidiEvent (Time 1) (Pitch (4, 3))
           , MidiEvent (Time 3) (Pitch (3,11))
           , MidiEvent (Time 4) (Pitch (4, 1))
           ]

-- | Transforming 'MidiData' into our feature representation is no trivial
test :: View v => v -> [Note v]
test v = select v testTune 

