module ZMidi.IO.NoteList ( readNoteListGeneric ) where

import ZMidi.Score
import Segment.SimpleNote          ( toTune, select, View, Note )
import ZMidi.IO.Common             ( warning, readMidiScore )
import System.FilePath             ( takeExtension )

import Data.Word                   ( Word8 )
import Data.Char                   ( toLower )
import Data.Csv
import Data.Vector (toList)
import qualified Data.ByteString.Lazy as BS (readFile)

readNoteListGeneric :: View -> FilePath -> IO [Note]
readNoteListGeneric v f = 
  do t <- case take 4 . map toLower . takeExtension $ f of
            ".mid" -> readMidiScore f >>= return . Right . toTune v
            ".txt" -> readNoteList  f >>= return . fmap (select v)
            ".csv" -> readNoteList  f >>= return . fmap (select v)
            e      -> error ("Error: " ++ e ++ " is not an accepted file type") 
            
     either (\x -> warning f x >> return []) return t
           
-- | Reads a CSV file "23,25,12\n..." and interprets it as a Timed ScoreEvent
-- The first number represents the onset, the second the off set, and the third
-- number represents pitch.
readNoteList :: FilePath -> IO (Either String [Timed ScoreEvent])
readNoteList fp =   BS.readFile fp 
                >>= return . fmap (toTimedScoreEvents . toList) . decode NoHeader

  where toTimedScoreEvents :: [(Int, Int, Word8)] -> [Timed ScoreEvent]
        toTimedScoreEvents = map toEvent where
          
          toEvent :: (Int, Int, Word8) -> Timed ScoreEvent
          toEvent (on, off, pch) =  
            Timed (Time on) (NoteEvent 0 (toPitch pch) 0 (Time (off - on)))