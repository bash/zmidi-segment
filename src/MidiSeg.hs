module Main where
 
import ZMidi.IO.Common                ( mapDir_ )
import ZMidi.IO.NoteList              ( readNoteListGeneric )
import Segment.SimpleNote
import Segment.RepPat

import System.Console.ParseArgs
import System.FilePath                ( (</>), (<.>), takeFileName )
import Data.Char                      ( toLower )

--------------------------------------------------------------------------------
-- Command-line argument parsing
--------------------------------------------------------------------------------
data MyArgs = ViewSelect | Mode | InputFilepath | InputDirFilepath | OutFilepath 
            | RepSelect  | RemoveOverlap | MinLength | BoundaryType   
               deriving (Eq, Ord, Show)

myArgs :: [Arg MyArgs]
myArgs = [
           Arg { argIndex = Mode,
                 argAbbr  = Just 'm',
                 argName  = Just "mode",
                 argData  = argDataRequired "mode" ArgtypeString,
                 argDesc  = "The operation mode (print|segment|reppat" ++
                   "                                        |notes|boundary)"
               }
        ,  Arg { argIndex = ViewSelect,
                 argAbbr  = Just 'v',
                 argName  = Just "view",
                 argData  = argDataRequired "view" ArgtypeString,
                 argDesc  = "The view (ioi|contour|pc|pitch|interval\n" ++ 
                   "                            |ioirat|pchXioi|intXioir)"
               }
        ,  Arg { argIndex = RepSelect,
                 argAbbr  = Just 'r',
                 argName  = Just "rep",
                 argData  = argDataDefaulted "rep" ArgtypeString "max",
                 argDesc  = "Repetition kind (max|supermax). Default: max"
               }
        ,  Arg { argIndex = RemoveOverlap,
                 argAbbr  = Just 'p',
                 argName  = Just "rem-olp",
                 argData  = argDataDefaulted "yes/no" ArgtypeString "no",
                 argDesc  = "REMOVE overlapping repetitions (no|yes) Default: no"
               }
        ,  Arg { argIndex = MinLength,
                 argAbbr  = Just 'l',
                 argName  = Just "length",
                 argData  = argDataDefaulted "length" ArgtypeInt 0,
                 argDesc  = "Minimal repetition length Default: 0"
               }
        ,   Arg { argIndex = OutFilepath,
                 argAbbr  = Just 'o',
                 argName  = Just "out",
                 argData  = argDataDefaulted "filepath" ArgtypeString "",
                 argDesc  = "Output directory for the segmentation files"
               }               
         , Arg { argIndex = InputFilepath,
                 argAbbr  = Just 'f',
                 argName  = Just "file",
                 argData  = argDataOptional "filepath" ArgtypeString,
                 argDesc  = "Input file midi file to analyse"
               }
         , Arg { argIndex = InputDirFilepath,
                 argAbbr  = Just 'd',
                 argName  = Just "dir",
                 argData  = argDataOptional "filepath" ArgtypeString,
                 argDesc  = "Base directory path containing only MIDI files"
               } 
         , Arg { argIndex = BoundaryType,
                 argAbbr  = Just 'b',
                 argName  = Just "bound",
                 argData  = argDataOptional "filepath" ArgtypeString,
                 argDesc  = "Selects the kind of boundary (start|end|both) " ++
                   "                                  in boundary mode"
               }                        
         ]

-- representing the mode of operation
data Mode = Print | Segment | Reppat | Notes | Boundary deriving (Eq)

-- Run from CL
main :: IO ()
main = do arg <- parseArgsIO ArgsComplete myArgs
          -- check whether we have a usable mode
          let mode   = case (map toLower $ getRequiredArg arg Mode) of
                         "print"   -> Print
                         "segment" -> Segment
                         "reppat"  -> Reppat
                         "notes"   -> Notes
                         "boundary"-> Boundary
                         m         -> usageError arg ("unrecognised mode: " ++ m)
              -- get parameters
              vw     = case (map toLower $ getRequiredArg arg ViewSelect) of
                         "ioi"      -> IOIView undefined
                         "contour"  -> CntView undefined
                         "pc"       -> PCView undefined
                         "pitch"    -> PchView undefined
                         "interval" -> IntView undefined
                         "ioirat"   -> RatView undefined
                         "pchxioi"  -> PchXIOIView undefined undefined
                         "intxioir" -> IntXIOIRatView undefined undefined
                         v         -> usageError arg ("unrecognised view: " ++ v)
                         
              c       = case (map toLower $ getRequiredArg arg RepSelect) of
                         "max"      -> Max
                         "supermax" -> SuperMax
                         v         -> usageError arg ("unrecognised repetition kind: " ++ v)
                         
              o       = case (map toLower $ getRequiredArg arg RemoveOverlap) of
                         "yes" -> RemOverlap True
                         "no"  -> RemOverlap False
                         v     -> usageError arg ("unrecognised overlap: " ++ v)
                         
              bf s    = case map toLower s of 
                         "start" -> Start
                         "end"   -> End
                         "both"  -> StartEnd
                         v       -> usageError arg ("unregonised boundary type: " ++ v)
                         
              l       = getRequiredArg arg MinLength
              out     = getRequiredArg arg OutFilepath :: FilePath
              b       = getOptReq arg BoundaryType "please provide a boundary type" bf
              
              -- the input is either a file (Left) or a directory (Right)
              input :: Either FilePath FilePath
              input = case ( getArg arg InputFilepath
                           , getArg arg InputDirFilepath ) of
                        -- we have two ways of identifying a file: by filename
                       (Just f , Nothing) -> Left f
                        -- or by basepath and id
                       (Nothing, Just d ) -> Right d
                       _ -> usageError arg "Please provide a file or directory" 
                  
          -- do the parsing magic
          case (mode, input) of
            (Print  , Left  f) -> printSegments l o c vw f 
            (Print  , Right d) -> mapDir_  (\f -> putStr (f ++ ": ") >> printSegments l o c vw f) d
            (Segment, Left  f) -> readRepPat l o c vw f >>= putStrLn . printSegmentLines . segment
            (Segment, Right d) -> mapDir_ (segmentDir (printSegmentLines . segment) l o c vw out) d
            (Reppat , Left  f) -> do putStrLn "noteNr.\tlength\tlabel\tpattern"
                                     readRepPat l o c vw f >>= putStrLn . printPatterns
            (Reppat , Right _) -> usageError arg "--mode reppat cannot be called on a directory"
            (Notes , Left  f ) -> readNoteListGeneric vw f >>= print
            (Notes , Right _ ) -> usageError arg "--mode notes cannot be called on a directory"
            (Boundary, Left f)  -> readRepPat l o c vw f >>= putStrLn . printRepBounds b
            (Boundary, Right d) -> mapDir_ (segmentDir (printRepBounds b) l o c vw out) d
            
readRepPat :: Int -> RemOverlap -> RepSelect -> View -> FilePath -> IO (Patterns Note)
-- readRepPat l o c v fp = readMidiScore fp >>= return . toRepPats l o c . toTune v
readRepPat l o c v fp = readNoteListGeneric v fp >>= return . toRepPats l o c 

printSegments :: Int -> RemOverlap -> RepSelect -> View -> FilePath -> IO ()
printSegments l o c vw f =   readRepPat l o c vw f 
                         >>= putStrLn . showSegmentationVerb . segment
            
segmentDir :: (Patterns Note -> String) 
           -> Int -> RemOverlap -> RepSelect -> View -> FilePath -> FilePath -> IO ()
segmentDir g l o c vw d f = 
  do let out = d </> takeFileName f <.> "out" <.> "txt" 
     readRepPat l o c vw f >>= writeFile out . g
     
-- | An optional argument that is required by certain program modes
getOptReq :: ArgType a => Args MyArgs -> MyArgs -> String -> (a -> b) -> b
getOptReq arg a s f = maybe (usageError arg s) f $ getArg arg a
