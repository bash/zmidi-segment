{-# OPTIONS_GHC -Wall                   #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ScopedTypeVariables        #-}
module Segment.SimpleNote ( Note (..)
                          , View (..)
                          , toTune
                          , select
                          ) where

-- zmidi stuff
import ZMidi.Score hiding  ( duration, toIOIs, Interval )

-- Other libs
import Data.List          ( intercalate )

--------------------------------------------------------------------------------
-- A representation for searching for patterns
--------------------------------------------------------------------------------

-- | The index of the 'Note'
type NoteIx = Int 

-- | A not consist of some specific view, or feature representation, derived 
-- from a 'MidiEvent', an onset, and an index
data Note = Note { nview     :: View 
                 , nonset    :: Time
                 , nNoteIx   :: NoteIx
                 } 

instance Show Note where
  show (Note v t ix) = show ix ++ " " ++ show t ++ ": " ++ show v 
  showList l s = ( intercalate "\n" . map show $ l ) ++ s

data View = IOIView IOI 
          | PCView PC 
          | IntView Interval 
          | PchView Pitch
          | CntView Contour
          | RatView IOIRatio
          | IntXIOIRatView Interval IOIRatio
          | PchXIOIView Pitch IOI
          deriving (Eq, Ord)
          
-- getView :: View -> a

data Dir     = Up | Down deriving (Eq, Ord)
data Contour = Prime | Step Dir | Leap Dir deriving (Eq, Ord)
                 
newtype Interval = Interval Int  deriving ( Eq, Num, Ord, Enum, Real, Integral )

newtype IOI      = IOI      Int  deriving ( Eq, Num, Ord, Enum, Real, Integral )

newtype IOIRatio = IOIRatio (Int, Int) deriving ( Eq, Ord )

-- | A Pitch class
newtype PC       = PC       Int  deriving ( Eq, Num, Ord, Enum, Real, Integral )
  
select :: View -> [Timed ScoreEvent] -> [Note]
select (PchView _) = toSingleViews pitchView
select (PCView  _) = toSingleViews pcView
select (IOIView _) = toCombViews ioiView
select (IntView _) = toCombViews intervalView
select (CntView _) = toCombViews contourView
select (RatView _) = toTripViews ioiRatioView
select (IntXIOIRatView _ _) = toTripViews intXDurRatView
select (PchXIOIView    _ _) = toCombViews pitchXIOIView

--------------------------------------------------------------------------------
-- Instances
--------------------------------------------------------------------------------
  
instance Eq Note where
  a == b = nview a == nview b  
 
instance Ord Note where
  compare a b = compare (nview a) (nview b)
  
instance Show Interval where
  show (Interval i) = show i
  
instance Show Dir where
  show Up   = "+"
  show Down = "-"
  
instance Show Contour where
  show Prime    = "p"
  show (Step d) = "s" ++ show d
  show (Leap d) = "l" ++ show d
  
instance Show PC where
  show (PC i) = show i
  
instance Show IOI where
  show (IOI i) = show i

instance Show IOIRatio where
  show (IOIRatio (a,b)) = show a ++ "/" ++ show b
  
instance Show View where
 show (PchView v) = show v
 show (PCView  v) = show v
 show (IOIView v) = show v
 show (IntView v) = show v
 show (CntView v) = show v
 show (RatView v) = show v
 show (IntXIOIRatView i r) = show i ++ "X" ++ show r
 show (PchXIOIView    p d) = show p ++ "X" ++ show d
 
--------------------------------------------------------------------------------
-- Pitch Classes
--------------------------------------------------------------------------------

pcView :: Timed ScoreEvent -> View
pcView = PCView . PC . pitchclass . getPC . getPitch

getPC :: Pitch -> PitchClass
getPC (Pitch (_oct, pc)) = pc

ioiView :: Timed ScoreEvent -> Timed ScoreEvent -> View
ioiView a b = IOIView . IOI . time $ (onset b - onset a)

intervalView :: Timed ScoreEvent -> Timed ScoreEvent -> View
intervalView a b = IntView $ toInterval (getPitch a) (getPitch b)

pitchView :: Timed ScoreEvent -> View
pitchView = PchView . getPitch

contourView :: Timed ScoreEvent -> Timed ScoreEvent -> View
contourView a b = CntView . intervalToStep $ toInterval (getPitch a) (getPitch b)

ioiRatioView :: Timed ScoreEvent -> Timed ScoreEvent -> Timed ScoreEvent -> View 
ioiRatioView a b c = RatView . IOIRatio $ ( time (onset b - onset a)
                                          , time (onset c - onset b))

intXDurRatView :: Timed ScoreEvent -> Timed ScoreEvent -> Timed ScoreEvent -> View 
intXDurRatView a b c = let i = toInterval (getPitch a) (getPitch b)
                           r = IOIRatio $ ( time (onset b - onset a)
                                          , time (onset c - onset b))
                       in IntXIOIRatView i r
                     
pitchXIOIView :: Timed ScoreEvent -> Timed ScoreEvent -> View 
pitchXIOIView a b = let p = getPitch a 
                        d = IOI . time $ (onset b - onset a)
                    in PchXIOIView p d
                                          
-- Merge these functions into one generic pattern
toSingleViews :: (Timed ScoreEvent -> View) -> [Timed ScoreEvent] -> [Note]
toSingleViews f = zipWith toSingleNote [0..] where

  toSingleNote :: NoteIx -> Timed ScoreEvent -> Note
  toSingleNote i e = Note (f e) (onset e) i 
  
toCombViews :: (Timed ScoreEvent -> Timed ScoreEvent -> View) -> [Timed ScoreEvent] -> [Note]
toCombViews g s = toCombViews' 0 s where

  toCombViews' :: NoteIx -> [Timed ScoreEvent] -> [Note]
  toCombViews' i [f , t]     = [toCombNote i f t]
  toCombViews' i (f : t : r) =  toCombNote i f t : toCombViews' (succ i) (t : r)
  toCombViews' _ _ = error "cannot calculate CombView for less than two notes" 

  toCombNote :: NoteIx -> Timed ScoreEvent -> Timed ScoreEvent -> Note
  toCombNote i fr to = Note (g fr to) (onset fr) i 
  
toTripViews :: (Timed ScoreEvent -> Timed ScoreEvent -> Timed ScoreEvent -> View)
            -> [Timed ScoreEvent] -> [Note ]
toTripViews g s = toTripViews' 0 s where

  toTripViews' :: NoteIx -> [Timed ScoreEvent] -> [Note]
  toTripViews' i [a , b , c]     = [toTripNote i a b c]
  toTripViews' i (a : b : c : r) =  toTripNote i a b c : toTripViews' (succ i) ( b : c : r)
  toTripViews' _ _ = error "cannot calculate CombView for less than three notes" 

  toTripNote :: NoteIx -> Timed ScoreEvent -> Timed ScoreEvent -> Timed ScoreEvent -> Note
  toTripNote i  a b c = Note (g a b c) (onset a) i 
  
--------------------------------------------------------------------------------
-- Contour
--------------------------------------------------------------------------------

-- zip [-5 .. 5] (map (intervalToStep . Interval) [-5 ..5])
-- [(-5,l-),(-4,l-),(-3,l-),(-2,s-),(-1,s-),(0,p),(1,s+),(2,s+),(3,l+),(4,l+),(5,l+)]
intervalToStep :: Interval -> Contour
intervalToStep (Interval i) 
  | i <= (-3) = Leap Down 
  | i <  0    = Step Down
  | i == 0    = Prime
  | i >= 3    = Leap Up
  | i >  0    = Step Up
  | otherwise = error "intervalToStep: impossible" 

--------------------------------------------------------------------------------
-- Intervals
--------------------------------------------------------------------------------

toInterval :: Pitch -> Pitch -> Interval
toInterval (Pitch (fo, fpc)) (Pitch (to, tpc)) = 
  Interval ((12 * (fromIntegral to  - fromIntegral fo )) 
                + (fromIntegral tpc - fromIntegral fpc))
           

--------------------------------------------------------------------------------
-- Utilities
--------------------------------------------------------------------------------

toTune :: View -> MidiScore -> [Note]
toTune v ms = case getVoices ms of
               [x] -> select v x
               _   -> error "toTune: I found more than one Voice (MIDI Track)" 

