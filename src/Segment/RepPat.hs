{-# OPTIONS_GHC -Wall                   #-}
{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Segment.RepPat ( Patterns (..)
                      , RepPat (..)
                      , RemOverlap (..)
                      , RepSelect (..)
                      , toRepPats
                      , printPatterns
                      , RepBound (..)
                      , printRepBounds
                      , segment
                      , showSegmentationVerb
                      , printSegmentLines
                      )where

import Segment.SimpleNote                       -- part of ZMidi.Score
import Segment.Frame            ( Frame (..), showSegmentationVerb )  -- part of RepPat

-- reppat stuff
import RepPat.RTree             ( createRTree, RTree )
import RepPat.RepPat            ( collectSuperMaxRep, collectMaxRep, selectRep
                                , Rep, simplifyAndRemOverlap, showRep
                                , filterMinRep )

import Data.Maybe               ( isJust )
import Data.List                ( intercalate, intersperse, find )
import Control.Monad.State.Lazy ( State, evalState, get, put )

data Patterns a = Patterns { patterns  :: [RepPat a]
                           , totLength :: Int 
                           } deriving (Functor, Eq, Show)

data RepPat a = RepPat { label :: (Char, Int)
                       , start :: NIx
                       , len   :: Int
                       , pat   :: [a]
                       } deriving (Functor, Eq, Show)

-- | Represents a note index
newtype NIx = NIx { nix :: Int } deriving (Eq, Show, Ord, Num, Enum, Real, Integral)
                       
newtype RemOverlap = RemOverlap Bool
data RepSelect = Max | SuperMax 
                       
-- | This is where the repeated pattern magic happens, we return a 
-- list of 'RepPat's
toRepPats :: Int -> RemOverlap -> RepSelect -> [Note] -> Patterns Note
toRepPats l o c xs = let rs = overlap o totLen . filterMinRep l . collect c 
                            . createRTree $ xs 

                         toRepPat :: Rep -> [a] -> RepPat a
                         toRepPat (srt, x, lab) p = RepPat lab (NIx srt) x p
                       
                         -- we start counting at 0
                         totLen = pred (length xs)
                       
                     in  Patterns (zipWith toRepPat rs . map (selectRep xs) $ rs)
                                  totLen 

-- | Transforms a 'RepPat' back to a 'Rep'
toReps :: [RepPat a] -> [Rep]
toReps = map (\(RepPat x (NIx s) l _) -> (s, l, x))
                                
-- | returns a simplification function based on 'RemOverlap'
overlap :: RemOverlap -> Int -> [Rep] -> [Rep]
overlap (RemOverlap b) l | b         = simplifyAndRemOverlap l
                         | otherwise = fill l

-- | fills all notes that are not part of a repetition with a "wild card 
-- repetition", marked with a '*'
fill :: Int -> [Rep] -> [Rep]
fill tl = fixStart . foldr step [] where

  step :: Rep -> [Rep] -> [Rep]
  step r []       = fixEnd r
  step a  (b: rs) = case gapCheck a b of
                      Nothing -> a : b : rs
                      Just i  -> a : i : b : rs

  gapCheck :: Rep -> Rep -> Maybe Rep
  gapCheck (sa, la, _) (sb, _lb, _) 
    | endA >= sb = Nothing
    | otherwise = Just (endA, sb - endA, ('*',0))
        where endA = sa + la
                              
  fixStart :: [Rep] -> [Rep]
  fixStart [] = []
  fixStart rs@((0,_,_) : _ ) = rs
  fixStart ( r@(s,_,_) : rs) = (0,s,('*',0)) : r : rs

  fixEnd :: Rep -> [Rep] 
  fixEnd r@(s, l, _) 
    | end  < tl = r : [(end, tl - end, ('*',0))]
    | otherwise =     [r]
        where end = s + l
                         
-- | Selects between maximal and super maximal repeats
collect :: (Show a, Ord a) => RepSelect -> RTree a -> [Rep]
collect SuperMax = collectSuperMaxRep
collect      Max = collectMaxRep
  

  
-- | Pretty prints NLB repeated patterns          
printPatterns :: Patterns Note -> String
printPatterns (Patterns p _ ) = (showRep . toReps $ p) ++ "\n" ++
                                (intercalate "\n" . map prnt $ p) where

  prnt :: RepPat Note  -> String
  prnt rp = intercalate "\t" [ show . nNoteIx . head . pat $ rp
                             , show . length . pat $ rp
                             , show . label $ rp
                             , show . map nview . pat $ rp ]

data RepBound = Start | End | StartEnd
          
-- | returns the start, end or start and end of a repetition boundary. Each
-- position in the list represents a note position. True represents a boundary          
repBounds :: RepBound -> Patterns a -> [Bool] 
repBounds b (Patterns ps l) = map repAtTime  [0 .. NIx l]

  where repAtTime :: NIx -> Bool
        repAtTime t = isJust $ find (bound b) ps
        
          where bound :: RepBound -> RepPat a -> Bool
                bound Start    p = noFill p &&  start p               == t
                bound End      p = noFill p &&  start p + NIx (len p) == t
                bound StartEnd p = noFill p && (start p + NIx (len p) == t || start p == t)
      
                -- | repetitions that "filled" gaps are ignored
                noFill :: RepPat a -> Bool
                noFill p = label p /= ('*',0)

-- | prints the repetition starts and endings
printRepBounds :: RepBound -> Patterns Note -> String 
printRepBounds b ps = (intersperse '\n' . map toChar $ repBounds b ps) ++ "\n"

  where toChar :: Bool -> Char
        toChar True  = '1'
        toChar False = '0'
                             
-- | Takes a list of 'Note's, analyses the repetitions within the chord
-- sequence, creates a segmentation based on these repetitions, and
-- outputs a final structural segmentation in the form of a list of 'Frame's
segment :: Patterns a -> [Frame]
segment (Patterns p l) = foldr toFrame [] p where
  
  -- | Transforms a 'RepPat', representing a segment (based on 
  -- repeated patterns), into a list of 'Frame's
  toFrame :: RepPat a -> [Frame] -> [Frame]
  toFrame r [] = map (Frame . label $ r) [nix (start r) .. l]
  toFrame r fs = map (Frame . label $ r) [nix (start r) .. (frameIx (head fs) - 1)] ++ fs

-- | Prints a '1' at at a segment boundary and a '0' otherwise
printSegmentLines :: [Frame] -> String
printSegmentLines fs = intersperse '\n' $ evalState (mapM step fs) '*' where

  step :: Frame -> State Char Char
  step f = do cur <- get
              let new = fst . branchLabel $ f
              put new
              -- if the branch label changes we create a segment 
              if cur == new || new == '*' then return '0' else return '1'
